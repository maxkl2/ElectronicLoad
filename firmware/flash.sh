#!/bin/sh

if [ -z "$1" ]; then
    echo "Usage: flash.sh <file>" >&2
    exit 1
fi

if [ -z "$2" ]; then
    target=stm32l0
else
    target="$2"
fi

filename="$1"

openocd -f /usr/share/openocd/scripts/interface/stlink-v2-1.cfg -f /usr/share/openocd/scripts/target/"$target".cfg \
    -c init -c targets -c halt \
    -c "flash write_image erase \"$filename\"" \
    -c "verify_image \"$filename\"" \
    -c "reset run" -c shutdown


#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/spi.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/exti.h>

// Reference voltage of ADC and DAC
#define VREF 2500 // mV

#define F_CPU 32000000 // Hz

#define CURRENT_MIN 10 // mA
#define CURRENT_MAX 4500 // mA
#define CURRENT_STEP 10 // mA
#define CURRENT_INITIAL 100 // mA

#define GPIO_CS_ADC GPIO8
#define GPIO_CS_DAC GPIO9
#define GPIO_CS_LCD GPIO10

#define GPIO_LCD_RS GPIO5

#define GPIO_ENC_A GPIO0
#define GPIO_ENC_B GPIO1
#define GPIO_ENC_BTN GPIO3

#define ADC_CHANNEL_CURRENT 0
#define ADC_CHANNEL_VOLTAGE 1
#define ADC_CHANNEL_TEMP 2

static void init_clock(void);
static void init_gpio(void);
static void init_spi(void);

static void delay_us(uint32_t us);
static void delay_short(void);

static void dac_write(uint16_t code);
static void dac_write_voltage(uint16_t millivolts);

static uint16_t adc_sample(uint8_t channel);
static uint16_t adc_sample_voltage(uint8_t channel);

static void lcd_init(void);
static void lcd_move_cursor(uint8_t line, uint8_t column);
static void lcd_write_char(char c);
static void lcd_write(char *text);
static void lcd_write_digit(uint8_t digit);

// Careful: load_set_current(...) is not affected by load_disable()
static void load_set_current(uint16_t milliamps);
static uint16_t load_measure_current(void);
static uint16_t load_measure_voltage(void);
static void load_enable(void);
static void load_disable(void);

// The main loop checks load_new_enabled and enables/disables the load when the two values differ
static bool load_enabled = false;
// This gets modified in the ISR for the rotary encoder button
static volatile bool load_new_enabled = false;

// The main loop checks load_new_selected_current and sets the load current when the two values differ
static uint16_t load_selected_current = CURRENT_INITIAL;
// This gets modified in the ISR for the rotary encoder
static volatile uint16_t load_new_selected_current = CURRENT_INITIAL;

static void delay_short(void) {
    for (int i = 0; i < 10; i++) {
        asm volatile ("nop");
    }
}

// spi_xfer(...) doesn't seem to work with the ADC, this fixed version clears the RX data register before the actual transfer
static uint16_t fixed_spi_xfer(uint32_t spi, uint16_t data) {
    // Clear pending RX data
    (void) SPI_DR(spi);

    // Transmit data when the previous transmission has finished
    spi_send(spi, data);

    // Read the received data when available
    return spi_read(spi);
}

int main(void) {
    init_clock();
    init_gpio();
    init_spi();

    // Set all CS signals high
    gpio_set(GPIOA, GPIO_CS_ADC | GPIO_CS_DAC | GPIO_CS_LCD);

    load_set_current(0);

    lcd_init();

    while (1) {
        if (load_enabled != load_new_enabled) {
            if (load_enabled) {
                load_disable();
            } else {
                load_enable();
            }
        }

        if (load_selected_current != load_new_selected_current) {
            if (load_enabled) {
                load_set_current(load_new_selected_current);
            }
            load_selected_current = load_new_selected_current;
        }

        bool enabled = load_enabled;
        uint16_t selected_current = load_selected_current;

        uint16_t current = load_measure_current();
        uint16_t voltage = load_measure_voltage();
        uint32_t power = ((uint32_t) current * voltage) / 1000;

        lcd_move_cursor(0, 2);
        lcd_write_digit((selected_current / 1000) % 10);
        lcd_write_char('.');
        lcd_write_digit((selected_current / 100) % 10);
        lcd_write_digit((selected_current / 10) % 10);
        lcd_write_char('A');

        lcd_move_cursor(0, 9);
        lcd_write_digit(voltage / 10000);
        lcd_write_digit((voltage / 1000) % 10);
        lcd_write_char('.');
        lcd_write_digit((voltage / 100) % 10);
        lcd_write_digit((voltage / 10) % 10);
        lcd_write_char('V');

        lcd_move_cursor(1, 1);
        lcd_write_digit(current / 10000);
        lcd_write_digit((current / 1000) % 10);
        lcd_write_char('.');
        lcd_write_digit((current / 100) % 10);
        lcd_write_digit((current / 10) % 10);
        lcd_write_char('A');

        lcd_move_cursor(1, 9);
        if (enabled) {
            lcd_write_digit(power / 100000);
            lcd_write_digit((power / 10000) % 10);
            lcd_write_digit((power / 1000) % 10);
            lcd_write_char('.');
            lcd_write_digit((power / 100) % 10);
            lcd_write_char('W');
        } else {
            lcd_write_char('-');
            lcd_write_char('O');
            lcd_write_char('F');
            lcd_write_char('F');
            lcd_write_char('-');
            lcd_write_char(' ');
        }
    }
}

static void init_clock(void) {
    // Run at 32 MHz (HSI16 + PLL)
    static const struct rcc_clock_scale clock_config = {
        .pll_source = RCC_CFGR_PLLSRC_HSI16_CLK,
        .pll_mul = RCC_CFGR_PLLMUL_MUL6,
        .pll_div = RCC_CFGR_PLLDIV_DIV3,
        .hpre = RCC_CFGR_HPRE_NODIV,
        .ppre1 = RCC_CFGR_PPRE1_NODIV,
        .ppre2 = RCC_CFGR_PPRE2_NODIV,
        .voltage_scale = PWR_SCALE1,
        .flash_waitstates = 1,
        .ahb_frequency  = 32000000,
        .apb1_frequency = 32000000,
        .apb2_frequency = 32000000,
    };
    rcc_clock_setup_pll(&clock_config);

    // 32 MHz / 8 = 4 MHz
    systick_set_clocksource(STK_CSR_CLKSOURCE_EXT);
    systick_set_reload((1 << 24) - 1);
    systick_counter_enable();

    rcc_periph_clock_enable(RCC_SYSCFG);
}

static void init_gpio(void) {
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);

    // SPI signals (SCLK, MISO, MOSI)
    gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO5 | GPIO7 | GPIO6);
    gpio_set_af(GPIOA, GPIO_AF0, GPIO5 | GPIO6 | GPIO7);
    // Workaround
    gpio_set_output_options(GPIOA, GPIO_OTYPE_PP, 0x3, GPIO5);
    // SPI CS signals
    gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_CS_ADC | GPIO_CS_DAC | GPIO_CS_LCD);

    gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_LCD_RS);

    // Rotary encoder
    gpio_mode_setup(GPIOB, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_ENC_A | GPIO_ENC_B | GPIO_ENC_BTN);
    // Setup interrupts for pin B (PB1) and the button (PB3)
    exti_select_source(EXTI1, GPIOB);
    exti_select_source(EXTI3, GPIOB);
    exti_set_trigger(EXTI1, EXTI_TRIGGER_FALLING);
    exti_set_trigger(EXTI3, EXTI_TRIGGER_FALLING);
    exti_enable_request(EXTI1);
    exti_enable_request(EXTI3);
    nvic_set_priority(NVIC_EXTI0_1_IRQ, 1);
    nvic_set_priority(NVIC_EXTI2_3_IRQ, 1);
    nvic_enable_irq(NVIC_EXTI0_1_IRQ);
    nvic_enable_irq(NVIC_EXTI2_3_IRQ);
}

static void init_spi(void) {
    rcc_periph_clock_enable(RCC_SPI1);

    spi_reset(SPI1);

    spi_set_master_mode(SPI1);
    // SPI clock = system clock / 64 = 500 kHz
    spi_set_baudrate_prescaler(SPI1, 0x4);
    spi_set_standard_mode(SPI1, 0);
    spi_set_dff_8bit(SPI1);
    spi_send_msb_first(SPI1);

    spi_enable_software_slave_management(SPI1);
    spi_set_nss_high(SPI1);

    spi_enable(SPI1);
}

static void delay_us(uint32_t us) {
    systick_clear();

    uint32_t end_count = (1 << 24) - 1 - us * 4;

    while (systick_get_value() > end_count) {
        // busy-wait
    }
}

static void dac_write(uint16_t code) {
    gpio_clear(GPIOA, GPIO_CS_DAC);

    delay_short();

    spi_send(SPI1, (0x7 << 4) | ((code >> 8) & 0xf));
    spi_send(SPI1, code & 0xff);

    while (SPI_SR(SPI1) & SPI_SR_BSY);

    delay_short();

    gpio_set(GPIOA, GPIO_CS_DAC);
}

static void dac_write_voltage(uint16_t millivolts) {
    // TODO: better resolution than millivolts
    // TODO: proper rounding
    uint32_t code = ((uint32_t) millivolts * 4096) / VREF;
    dac_write(code > 4095 ? 4095 : code);
}

static uint16_t adc_sample(uint8_t channel) {
    gpio_clear(GPIOA, GPIO_CS_ADC);

    delay_short();

    uint16_t result_high, result_low;

    fixed_spi_xfer(SPI1, 0x06);
    result_high = fixed_spi_xfer(SPI1, (channel & 0x3) << 6);
    result_low = fixed_spi_xfer(SPI1, 0x00);

    delay_short();

    gpio_set(GPIOA, GPIO_CS_ADC);

    uint16_t result = ((uint16_t) (result_high & 0x0f) << 8) | result_low;

    return result;
}

static uint16_t adc_sample_voltage(uint8_t channel) {
    // TODO: better resolution than millivolts
    // TODO: proper rounding
    uint16_t result = adc_sample(channel);
    return ((uint32_t) result * VREF) / 4096;
}

static void lcd_send(bool rs, uint8_t data) {
    gpio_clear(GPIOA, GPIO_CS_LCD);

    if (rs) {
        gpio_set(GPIOB, GPIO_LCD_RS);
    } else {
        gpio_clear(GPIOB, GPIO_LCD_RS);
    }

    delay_us(100);

    spi_send(SPI1, data);

    delay_us(200);

    gpio_set(GPIOA, GPIO_CS_LCD);
}

static void lcd_init(void) {
    gpio_clear(GPIOB, GPIO_LCD_RS);

    // Let LCD controller reset itself
    delay_us(40000);

    // Function Set: 8 bit, 2 rows, instruction table 1
    lcd_send(false, 0x39);
    delay_us(30);
    lcd_send(false, 0x39);
    delay_us(30);
    // Bias Set: bias 1/5, 2 rows
    lcd_send(false, 0x14);
    delay_us(30);
    // Power Control: voltage booster on, set contrast bits C4-C5
    lcd_send(false, 0x55);
    delay_us(30);
    // Follower Control: voltage follower on, set amplification
    lcd_send(false, 0x6d);
    delay_us(30);
    // Contrast Set: set contrast bits C1-C3
    lcd_send(false, 0x78);
    delay_us(30);

    // Wait for power to stabilize
    delay_us(50000);

    // Function Set: back to instruction table 0
    lcd_send(false, 0x38);
    delay_us(30);
    // Display ON/OFF: display on
    lcd_send(false, 0x0c);
    delay_us(30);
    // Clear Display
    lcd_send(false, 0x01);
    delay_us(30);
    // Entry Mode Set: auto-increment cursor, don't shift
    lcd_send(false, 0x06);
    delay_us(30);
}

static void lcd_move_cursor(uint8_t line, uint8_t column) {
    if (line > 1 || column > 15) {
        return;
    }

    uint8_t addr = line * 0x40 + column;
    // Set DDRAM address
    lcd_send(false, 0x80 | addr);
    delay_us(30);
}

static void lcd_write_char(char c) {
    // Write data to RAM
    lcd_send(true, (uint8_t) c);
    delay_us(30);
}

static void lcd_write(char *text) {
    while (*text) {
        lcd_write_char(*text++);
    }
}

static void lcd_write_digit(uint8_t digit) {
    if (digit > 9) {
        return;
    }

    lcd_write_char(digit + '0');
}

static void load_set_current(uint16_t milliamps) {
    // TODO: precision
    dac_write_voltage(milliamps / 2);
}

static uint16_t load_measure_current(void) {
    // TODO: precision
    return adc_sample_voltage(0) * 2;
}

static uint16_t load_measure_voltage(void) {
    // TODO: precision
    return adc_sample_voltage(1) * 20;
}

static void load_enable(void) {
    if (load_enabled) {
        return;
    }

    // Restore the current that was set before disabling the load
    load_set_current(load_selected_current);

    load_enabled = true;
}

static void load_disable(void) {
    if (!load_enabled) {
        return;
    }

    // Fully open the load transistor
    load_set_current(0);

    load_enabled = false;
}

// Interrupt service routines

// ISR for the rotary encoder on PB0+1
void exti0_1_isr(void) {
    exti_reset_request(EXTI1);

    // The interrupt triggers on a falling edge of pin B,
    //  we just need to check the level of pin A to determine the turning direction.
    //  The RC filter between rotary encoder and GPIO pin seems to eliminate the need for any debouncing
    if (gpio_get(GPIOB, GPIO_ENC_A)) {
        if (load_new_selected_current > CURRENT_MIN) {
            load_new_selected_current -= CURRENT_STEP;
        }
    } else {
        if (load_new_selected_current < CURRENT_MAX) {
            load_new_selected_current += CURRENT_STEP;
        }
    }
}

// ISR for the rotary encoder button on PB3
void exti2_3_isr(void) {
    exti_reset_request(EXTI3);

    load_new_enabled = !load_new_enabled;
}

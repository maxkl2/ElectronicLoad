
# CMake toolchain file for STM32 with libopencm3
#
# Usage:
#
#   Initialising CMake (example):
#     cmake ../.. -DCMAKE_TOOLCHAIN_FILE=../../stm32.cmake -DCMAKE_BUILD_TYPE=Debug -DSTM32_MCU=stm32f446re -DSTM32_FAMILY=stm32f4 -DSTM32_CPU=cortex-m4
#
#   Specify a target using libopencm3:
#     stm32_use_libopencm3(<target>)

set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR stm32)
set(CMAKE_CROSSCOMPILING 1)

function(define_cache_var varname default desc)
    if(DEFINED ENV{_${varname}})
        set(tmp "$ENV{_${varname}}")
    elseif(${varname})
        set(tmp "${${varname}}")
    else()
        if(default)
            set(tmp "${default}")
        else()
            message(FATAL_ERROR "${varname} not set")
        endif()
    endif()
    set(${varname} "${tmp}" CACHE STRING "${desc}")
    set(ENV{_${varname}} "${tmp}")
endfunction()

define_cache_var(STM32_COMPILER_PREFIX "arm-none-eabi-" "Compiler prefix")
define_cache_var(STM32_MCU "" "Target microcontroller name, e.g. stm32f446re")
define_cache_var(STM32_FAMILY "" "Target microcontroller family, e.g. stm32f4")
define_cache_var(STM32_CPU "" "Target ARM CPU name, e.g. cortex-m4")
define_cache_var(LIBOPENCM3_PATH "libopencm3" "Path to libopencm3")

string(TOLOWER ${STM32_MCU} MCU_LOWER)
string(TOUPPER ${STM32_MCU} MCU_UPPER)
string(TOLOWER ${STM32_FAMILY} FAMILY_LOWER)
string(TOUPPER ${STM32_FAMILY} FAMILY_UPPER)
string(TOLOWER ${STM32_CPU} CPU_LOWER)
string(TOUPPER ${STM32_CPU} CPU_UPPER)

get_filename_component(LIBOPENCM3_PATH_ABSOLUTE "${LIBOPENCM3_PATH}" REALPATH BASE_DIR "${CMAKE_SOURCE_DIR}")
set(LINKER_SCRIPT "${CMAKE_SOURCE_DIR}/${MCU_LOWER}.ld")

set(CMAKE_C_COMPILER "${STM32_COMPILER_PREFIX}gcc")
set(CMAKE_CXX_COMPILER "${STM32_COMPILER_PREFIX}g++")

if(NOT GCC_FLAGS_SET)

    set(GCC_FLAGS "-mcpu=${CPU_LOWER} -mthumb")
    set(LD_FLAGS "-static -nostartfiles -Wl,--gc-sections -T ${LINKER_SCRIPT}")

    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${GCC_FLAGS}" CACHE STRING "" FORCE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GCC_FLAGS}" CACHE STRING "" FORCE)
    set(CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS} ${GCC_FLAGS}" CACHE STRING "" FORCE)
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${LD_FLAGS}" CACHE STRING "" FORCE)

    set(GCC_FLAGS_SET "true" CACHE INTERNAL "" FORCE)
endif()

link_directories(${target} "${LIBOPENCM3_PATH_ABSOLUTE}/lib")

function(stm32_use_libopencm3 target)
    target_include_directories(${target} PRIVATE "${LIBOPENCM3_PATH_ABSOLUTE}/include")
    target_link_libraries(${target} "opencm3_${FAMILY_LOWER}")
    target_compile_definitions(${target} PRIVATE -D${FAMILY_UPPER} -D${MCU_UPPER})
endfunction()

set(CMAKE_C_COMPILER_WORKS 1)
set(CMAKE_CXX_COMPILER_WORKS 1)
set(CMAKE_ASM_COMPILER_WORKS 1)

EESchema Schematic File Version 4
LIBS:Front-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Front-rescue:LED D1
U 1 1 5ABA6958
P 6100 4400
F 0 "D1" H 6100 4500 50  0000 C CNN
F 1 "LED_ACTIVE" H 6100 4300 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H 6100 4400 50  0001 C CNN
F 3 "" H 6100 4400 50  0001 C CNN
	1    6100 4400
	0    -1   -1   0   
$EndComp
$Comp
L Front-rescue:Rotary_Encoder_Switch SW1
U 1 1 5ABA6966
P 4750 4650
F 0 "SW1" H 4750 4910 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 4750 4390 50  0000 C CNN
F 2 "footprints:Bourns_PEC12R-3xxxF-Sxxxx" H 4650 4810 50  0001 C CNN
F 3 "" H 4750 4910 50  0001 C CNN
	1    4750 4650
	-1   0    0    -1  
$EndComp
$Comp
L Front-rescue:R R1
U 1 1 5ABA696D
P 6100 4750
F 0 "R1" V 6180 4750 50  0000 C CNN
F 1 "590" V 6100 4750 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6030 4750 50  0001 C CNN
F 3 "" H 6100 4750 50  0001 C CNN
	1    6100 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5ABA697B
P 6100 4950
F 0 "#PWR01" H 6100 4700 50  0001 C CNN
F 1 "GND" H 6100 4800 50  0000 C CNN
F 2 "" H 6100 4950 50  0001 C CNN
F 3 "" H 6100 4950 50  0001 C CNN
	1    6100 4950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5ABA6981
P 4400 5000
F 0 "#PWR02" H 4400 4750 50  0001 C CNN
F 1 "GND" H 4400 4850 50  0000 C CNN
F 2 "" H 4400 5000 50  0001 C CNN
F 3 "" H 4400 5000 50  0001 C CNN
	1    4400 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5ABA6987
P 5100 5000
F 0 "#PWR03" H 5100 4750 50  0001 C CNN
F 1 "GND" H 5100 4850 50  0000 C CNN
F 2 "" H 5100 5000 50  0001 C CNN
F 3 "" H 5100 5000 50  0001 C CNN
	1    5100 5000
	1    0    0    -1  
$EndComp
$Comp
L CustomComponents:EA_DOGMxxx-A DS1
U 1 1 5ABA698D
P 3050 2950
F 0 "DS1" H 2750 3750 50  0000 C CNN
F 1 "EA_DOGMxxx-A" H 3350 3750 50  0000 C CNN
F 2 "footprints:EA_DOGMxxx-A" H 3020 1980 50  0001 C CNN
F 3 "" H 3500 2600 50  0001 C CNN
	1    3050 2950
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5ABA6994
P 3050 3800
F 0 "#PWR04" H 3050 3550 50  0001 C CNN
F 1 "GND" H 3050 3650 50  0000 C CNN
F 2 "" H 3050 3800 50  0001 C CNN
F 3 "" H 3050 3800 50  0001 C CNN
	1    3050 3800
	-1   0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR05
U 1 1 5ABA699A
P 3650 3150
F 0 "#PWR05" H 3650 3000 50  0001 C CNN
F 1 "+3V3" H 3650 3290 50  0000 C CNN
F 2 "" H 3650 3150 50  0001 C CNN
F 3 "" H 3650 3150 50  0001 C CNN
	1    3650 3150
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5ABA69A0
P 3600 2600
F 0 "#PWR06" H 3600 2350 50  0001 C CNN
F 1 "GND" H 3600 2450 50  0000 C CNN
F 2 "" H 3600 2600 50  0001 C CNN
F 3 "" H 3600 2600 50  0001 C CNN
	1    3600 2600
	0    -1   1    0   
$EndComp
$Comp
L power:+3V3 #PWR07
U 1 1 5ABA69A6
P 3600 2500
F 0 "#PWR07" H 3600 2350 50  0001 C CNN
F 1 "+3V3" H 3600 2640 50  0000 C CNN
F 2 "" H 3600 2500 50  0001 C CNN
F 3 "" H 3600 2500 50  0001 C CNN
	1    3600 2500
	0    1    -1   0   
$EndComp
$Comp
L power:+3V3 #PWR08
U 1 1 5ABA69AC
P 2500 2500
F 0 "#PWR08" H 2500 2350 50  0001 C CNN
F 1 "+3V3" H 2500 2640 50  0000 C CNN
F 2 "" H 2500 2500 50  0001 C CNN
F 3 "" H 2500 2500 50  0001 C CNN
	1    2500 2500
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5ABA69B2
P 2500 2300
F 0 "#PWR09" H 2500 2050 50  0001 C CNN
F 1 "GND" H 2500 2150 50  0000 C CNN
F 2 "" H 2500 2300 50  0001 C CNN
F 3 "" H 2500 2300 50  0001 C CNN
	1    2500 2300
	0    1    -1   0   
$EndComp
$Comp
L power:+3V3 #PWR010
U 1 1 5ABA69B8
P 3050 1800
F 0 "#PWR010" H 3050 1650 50  0001 C CNN
F 1 "+3V3" H 3050 1940 50  0000 C CNN
F 2 "" H 3050 1800 50  0001 C CNN
F 3 "" H 3050 1800 50  0001 C CNN
	1    3050 1800
	-1   0    0    -1  
$EndComp
$Comp
L Front-rescue:C C3
U 1 1 5ABA69BE
P 2800 1850
F 0 "C3" H 2825 1950 50  0000 L CNN
F 1 "100n" H 2825 1750 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2838 1700 50  0001 C CNN
F 3 "" H 2800 1850 50  0001 C CNN
	1    2800 1850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5ABA69C5
P 2550 1900
F 0 "#PWR011" H 2550 1650 50  0001 C CNN
F 1 "GND" H 2550 1750 50  0000 C CNN
F 2 "" H 2550 1900 50  0001 C CNN
F 3 "" H 2550 1900 50  0001 C CNN
	1    2550 1900
	-1   0    0    -1  
$EndComp
$Comp
L Front-rescue:C C2
U 1 1 5ABA69CB
P 2050 2750
F 0 "C2" H 2075 2850 50  0000 L CNN
F 1 "1u" H 2075 2650 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2088 2600 50  0001 C CNN
F 3 "" H 2050 2750 50  0001 C CNN
	1    2050 2750
	1    0    0    -1  
$EndComp
$Comp
L Front-rescue:C C1
U 1 1 5ABA69D2
P 1800 3200
F 0 "C1" H 1825 3300 50  0000 L CNN
F 1 "1u" H 1825 3100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1838 3050 50  0001 C CNN
F 3 "" H 1800 3200 50  0001 C CNN
	1    1800 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR012
U 1 1 5ABA69D9
P 1800 2950
F 0 "#PWR012" H 1800 2800 50  0001 C CNN
F 1 "+3V3" H 1800 3090 50  0000 C CNN
F 2 "" H 1800 2950 50  0001 C CNN
F 3 "" H 1800 2950 50  0001 C CNN
	1    1800 2950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4400 4550 4450 4550
Wire Wire Line
	4400 3700 4400 4550
Wire Wire Line
	6100 3800 6100 4250
Wire Wire Line
	6100 4550 6100 4600
Wire Wire Line
	6100 4900 6100 4950
Wire Wire Line
	4400 5000 4400 4750
Wire Wire Line
	4400 4750 4450 4750
Wire Wire Line
	5100 5000 5100 4650
Wire Wire Line
	5100 4650 5050 4650
Wire Wire Line
	3600 2500 3500 2500
Wire Wire Line
	3600 2600 3500 2600
Wire Wire Line
	2600 2500 2500 2500
Wire Wire Line
	2600 2300 2500 2300
Wire Wire Line
	3500 3600 4100 3600
Wire Wire Line
	3500 3500 4000 3500
Wire Wire Line
	3500 2300 4100 2300
Wire Wire Line
	3500 2700 4000 2700
Wire Wire Line
	3050 1800 3050 1850
Wire Wire Line
	2950 1850 3050 1850
Connection ~ 3050 1850
Wire Wire Line
	2650 1850 2550 1850
Wire Wire Line
	2550 1850 2550 1900
Wire Wire Line
	2050 2600 2050 2550
Wire Wire Line
	2050 2550 2250 2550
Wire Wire Line
	2250 2550 2250 2700
Wire Wire Line
	2250 2700 2600 2700
Wire Wire Line
	2050 2900 2050 2950
Wire Wire Line
	2050 2950 2250 2950
Wire Wire Line
	2250 2950 2250 2800
Wire Wire Line
	2250 2800 2600 2800
Wire Wire Line
	2600 3000 1800 3000
Wire Wire Line
	1800 2950 1800 3000
Connection ~ 1800 3000
Wire Wire Line
	2600 3100 2000 3100
Wire Wire Line
	1800 3400 1800 3350
Wire Wire Line
	2000 3400 1800 3400
Wire Wire Line
	2000 3100 2000 3400
Wire Wire Line
	5050 4750 5300 4750
Wire Wire Line
	3600 2900 3600 3000
Wire Wire Line
	3600 2900 3500 2900
Wire Wire Line
	3600 3400 3500 3400
Wire Wire Line
	3650 3150 3600 3150
Connection ~ 3600 3150
Wire Wire Line
	3500 3000 3600 3000
Connection ~ 3600 3000
Wire Wire Line
	3500 3100 3600 3100
Connection ~ 3600 3100
Wire Wire Line
	3500 3200 3600 3200
Connection ~ 3600 3200
Wire Wire Line
	3500 3300 3600 3300
Connection ~ 3600 3300
Wire Wire Line
	5050 4550 5200 4550
$Comp
L Front-rescue:Conn_01x10 J1
U 1 1 5ABA8201
P 6550 3300
F 0 "J1" H 6550 3900 50  0000 C CNN
F 1 "CON_MAIN_BOARD" H 6550 2700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x05_Pitch2.54mm" H 6550 3300 50  0001 C CNN
F 3 "" H 6550 3300 50  0001 C CNN
	1    6550 3300
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR013
U 1 1 5ABA8402
P 5900 2900
F 0 "#PWR013" H 5900 2750 50  0001 C CNN
F 1 "+3V3" H 5900 3040 50  0000 C CNN
F 2 "" H 5900 2900 50  0001 C CNN
F 3 "" H 5900 2900 50  0001 C CNN
	1    5900 2900
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5ABA842A
P 5900 3000
F 0 "#PWR014" H 5900 2750 50  0001 C CNN
F 1 "GND" H 5900 2850 50  0000 C CNN
F 2 "" H 5900 3000 50  0001 C CNN
F 3 "" H 5900 3000 50  0001 C CNN
	1    5900 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	5900 2900 6000 2900
Wire Wire Line
	5900 3000 6250 3000
Wire Wire Line
	4100 2300 4100 3100
Wire Wire Line
	4100 3100 6350 3100
Wire Wire Line
	4000 2700 4000 3200
Wire Wire Line
	4000 3200 6350 3200
Wire Wire Line
	4000 3500 4000 3300
Wire Wire Line
	4000 3300 6350 3300
Wire Wire Line
	4100 3600 4100 3400
Wire Wire Line
	4100 3400 6350 3400
Wire Wire Line
	5200 4550 5200 3500
Wire Wire Line
	5200 3500 6350 3500
Wire Wire Line
	5300 4750 5300 3600
Wire Wire Line
	5300 3600 6350 3600
Wire Wire Line
	4400 3700 6350 3700
Wire Wire Line
	6100 3800 6350 3800
$Comp
L power:PWR_FLAG #FLG015
U 1 1 5ABB1CF6
P 6000 2750
F 0 "#FLG015" H 6000 2825 50  0001 C CNN
F 1 "PWR_FLAG" H 6000 2900 50  0000 C CNN
F 2 "" H 6000 2750 50  0001 C CNN
F 3 "" H 6000 2750 50  0001 C CNN
	1    6000 2750
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG016
U 1 1 5ABB1D20
P 6250 2850
F 0 "#FLG016" H 6250 2925 50  0001 C CNN
F 1 "PWR_FLAG" H 6250 3000 50  0000 C CNN
F 2 "" H 6250 2850 50  0001 C CNN
F 3 "" H 6250 2850 50  0001 C CNN
	1    6250 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 2750 6000 2900
Connection ~ 6000 2900
Wire Wire Line
	6250 2850 6250 3000
Connection ~ 6250 3000
Wire Wire Line
	3050 1850 3050 2100
Wire Wire Line
	1800 3000 1800 3050
Wire Wire Line
	3600 3150 3600 3200
Wire Wire Line
	3600 3000 3600 3100
Wire Wire Line
	3600 3100 3600 3150
Wire Wire Line
	3600 3200 3600 3300
Wire Wire Line
	3600 3300 3600 3400
Wire Wire Line
	6000 2900 6350 2900
Wire Wire Line
	6250 3000 6350 3000
$EndSCHEMATC

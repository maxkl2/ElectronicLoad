EESchema Schematic File Version 4
LIBS:Main-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4000 3700 1300 1250
U 5AB675B6
F0 "Power" 60
F1 "power.sch" 60
$EndSheet
$Sheet
S 5850 3700 1300 1250
U 5AB6B718
F0 "Digital" 60
F1 "digital.sch" 60
F2 "SPI_MOSI" O R 7150 3950 60 
F3 "SPI_MISO" I R 7150 4050 60 
F4 "SPI_SCLK" O R 7150 4150 60 
F5 "SPI_CS_DAC" O R 7150 4250 60 
F6 "SPI_CS_ADC" O R 7150 4350 60 
$EndSheet
$Sheet
S 7600 3700 1350 1250
U 5AB75388
F0 "Analog" 60
F1 "analog.sch" 60
F2 "SPI_MOSI" I L 7600 3950 60 
F3 "SPI_MISO" O L 7600 4050 60 
F4 "SPI_SCLK" I L 7600 4150 60 
F5 "SPI_CS_DAC" I L 7600 4250 60 
F6 "SPI_CS_ADC" I L 7600 4350 60 
$EndSheet
Wire Wire Line
	7150 3950 7600 3950
Wire Wire Line
	7150 4050 7600 4050
Wire Wire Line
	7150 4150 7600 4150
Wire Wire Line
	7150 4250 7600 4250
Wire Wire Line
	7150 4350 7600 4350
$EndSCHEMATC
